﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class LargestProduct
    {
        String path;
        public LargestProduct(String path)
        {
            this.path = path;
        }





        /// <summary>
        /// This method will take in a separator and will then break up the file into a 
        /// 2D int array. 
        /// </summary>
        /// <param name="separator"></param>
        /// <returns></returns>
        private int[][] TransformFile(Char separator)
        {
            //First transform the file into a String array, each part of the array contains a line of the file
            String[] lines = System.IO.File.ReadAllLines(path);

            //Then convert it into a 2D String array
            String[][] words = new String[lines.Length][];

            for (int i = 0; i < lines.Length; i++)
            {
                words[i] = lines[i].Split(separator);

                if (lines.Length != words[i].Length)
                    throw new System.ArgumentException();
            }

            //Transform the 2D String array into a 2D int array
            int[][] returnArray = new int[words.Length][];

            for (int i = 0; i < words.Length; i++)
            {
                returnArray[i] = new int[words[i].Length];

                for (int j = 0; j < words[i].Length; j++)
                {
                    returnArray[i][j] = int.Parse(words[i][j]);
                }
            }

            return returnArray;
        }



        
        public long maxProductOfGrid(int adjacentBlocks)
        {
            return maxProduct(TransformFile(' '), adjacentBlocks);
        }


        private long maxProduct(int[][] grid, int adjacentBlocks)
        {
            long max = 0;

            long maxHor = CalculateHorizontal(grid, adjacentBlocks);
            long maxVer = CalculateVertical(grid, adjacentBlocks);
            long maxLeftDiagonal = CalculateLeftDiagonal(grid, adjacentBlocks);
            long maxRightDiagonal = CalculateRightDiagonal(grid, adjacentBlocks);

            if (maxHor > max)
                max = maxHor;

            if (maxVer > max)
                max = maxVer;

            if (maxLeftDiagonal > max)
                max = maxLeftDiagonal;

            if (maxRightDiagonal > max)
                max = maxRightDiagonal;

            return max;

        }

        private long CalculateHorizontal(int[][] grid, int adjacentBlocks)
        {
            long max = 0;
            
            for (int ver = 0; ver < grid.Length; ver++)
            {
                for (int hor = 0; hor + adjacentBlocks <= grid[ver].Length; hor++)
                {
                    long product = 1;

                    for (int x = hor; x < hor + adjacentBlocks; x++)
                    {
                        product = grid[ver][x] * product; 
                    }

                    if (product > max)
                        max = product;
                }
            }

            return max;
        }



        private long CalculateVertical(int[][] grid, int adjacentBlocks)
        {
            long max = 0;

           for (int hor = 0; hor < grid.Length; hor++)
            {
                for (int ver = 0; ver + adjacentBlocks <= grid.Length; ver++)
                {
                    long product = 1;
                    
                    for (int x = ver; x < ver + adjacentBlocks; x++)
                    {
                        product = grid[x][hor] * product;
                    }

                    if (product > max)
                        max = product;
                }
            }

            return max;
        }


        private long CalculateLeftDiagonal(int[][] grid, int adjacentBlocks)
        {
            long max = 0;

            for (int ver = 0; ver < grid.Length - adjacentBlocks + 1; ver++)
            {
                for (int hor = 0; hor < grid.Length - adjacentBlocks + 1; hor++)
                {
                    long product = 1;

                    for (int x = 0; x < adjacentBlocks; x++)
                    {
                        product = grid[ver + x][hor + x] * product;
                    }

                    if (product > max)
                        max = product;
                }
            }

            return max; 
        }


        private long CalculateRightDiagonal(int[][] grid, int adjacentBlocks)
        {
            //Create a new flipped grid
            int[][] newGrid = new int[grid.Length][];

            for(int i = 0; i < grid.Length; i++)
            {
                newGrid[i] = new int[grid[i].Length];
                for (int j =0; j < grid[i].Length; j++)
                {
                    newGrid[i][j] = grid[i][grid.Length - j - 1];
                }
            }


            return CalculateLeftDiagonal(newGrid, adjacentBlocks);
        }


    }
}
