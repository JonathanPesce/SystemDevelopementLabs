﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PongLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Pong
{
    public class BallSprite : DrawableGameComponent
    {
        private Ball ball;

        //To render
        private SpriteBatch spriteBatch;
        private Texture2D imageBall;
        private Game1 game;
        private Paddle paddle;

        public Paddle Paddle{ set { paddle = value; }  }

        public BallSprite(Game1 game) :base(game)
        {
            this.game = game;
        }

        public override void Initialize()
        {

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            imageBall = game.Content.Load<Texture2D>("ball");
            ball = new Ball(imageBall.Width, imageBall.Height, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height, 2, 2, paddle);
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            ball.Move();
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(imageBall, new Vector2(ball.BoundingBox.X, ball.BoundingBox.Y), Color.White);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
