﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7Part1
{
    public class CompareByName : IStudentComparer
    {
        public int CompareTo(Student one, Student two)
        {
            return one.Name.CompareTo(two.Name);
        }
    }
}
