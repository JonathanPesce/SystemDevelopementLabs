﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicDesign
{
    public class Program
    {
        static void Main(string[] args)
        {
            Graphic big = new Graphic("House");
            Graphic roof = new Graphic("Roof");
            Graphic window = new Graphic("Window");

            Shape triangle = new Shape("triangle");
            Shape rectangle = new Shape("rectangle");
            Shape circle = new Shape("circle");
            Shape lines = new Shape("lines");

            big.AddComponent(roof);
            big.AddComponent(window);
            roof.AddComponent(triangle);
            roof.AddComponent(rectangle);
            window.AddComponent(circle);
            window.AddComponent(lines);

            big.Display();

        }
    }
}
