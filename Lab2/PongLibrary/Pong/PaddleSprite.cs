﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PongLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Pong
{
    public class PaddleSprite : DrawableGameComponent
    {
        private Paddle paddle;
        //the business logic
        //to render
        private SpriteBatch spriteBatch;
        private Texture2D imagePaddle;
        private Game1 game;
        
        //keyboard input
        private KeyboardState oldState;
        private int counter;
        private int threshold;

        public Paddle PaddleObject { get { return paddle; } }

        public PaddleSprite(Game1 game):base(game)
        {
            this.game = game;
            this.paddle = new Paddle(592, 170, 1000, 1000, 5);

         //   this.paddle = new Paddle(592, 170, 200, 200, 5);
        }

        public override void Initialize()
        {
            this.oldState = Keyboard.GetState();
            this.threshold = 6;

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            imagePaddle = game.Content.Load<Texture2D>("paddle");
       //     this.paddle = new Paddle(imagePaddle.Width, imagePaddle.Height, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height, 5);
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            checkInput();
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(imagePaddle, new Vector2(paddle.BoundingBox.X, paddle.BoundingBox.Y), Color.White);
            spriteBatch.End();
            base.Draw(gameTime);
        }

        private void checkInput()
        {
            KeyboardState newState = Keyboard.GetState();
            if (newState.IsKeyDown(Keys.Right))
            {
                // If not down last update, key has just been pressed.
                if (!oldState.IsKeyDown(Keys.Right))
                {
                    this.paddle.MoveRight();
                    this.counter = 0; //reset counter with every new keystroke
                }
                else
                {
                    this.counter++;
                    if (this.counter > threshold)
                        this.paddle.MoveRight();
                }
            }

            if (newState.IsKeyDown(Keys.Left))
            {
                if (!oldState.IsKeyDown(Keys.Left))
                {
                    this.paddle.MoveLeft();
                    this.counter = 0; 
                }
                else
                {
                    this.counter++;
                    if (this.counter > threshold)
                        this.paddle.MoveLeft();
                }
            }

            // Once finished checking all keys, update old state.
            oldState = newState;

        }
    }
}