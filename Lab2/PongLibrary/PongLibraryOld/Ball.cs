﻿using System;
using Microsoft.Xna.Framework;

namespace PongLibrary
{
    public class Ball
    {
        private Vector2 velocity;
        private Rectangle boundingBall;
        private Rectangle screen;
        private Paddle paddle;

        public Ball(int ballWidth, int ballHeight, int screenWidth, int screenHeight, int speedX, int speedY, Paddle paddle)
        {

            velocity = new Vector2(speedX, speedY);

            int x = (screenWidth / 2) - (ballWidth / 2);
            boundingBall = new Rectangle(x, 0, ballWidth, ballHeight);

            screen = new Rectangle(0, 0, screenWidth, screenHeight);

            this.paddle = paddle;

        }

        /// <summary>
        /// This is a property for the variable boundingBox. This proprety has a private
        /// set and a public get, that returns the value of the Rectangle boundingBall. 
        /// </summary>
        public Rectangle BoundingBox
        {

            get
            {
                return boundingBall;
            }

        }


        /// <summary>
        /// This method will Move the ball depending on it's velocity. The method will ensure
        /// that the ball does not leave the screen.
        /// </summary>
        public void Move()
        {
            MoveX();
            MoveY();
        }

        /// <summary>
        /// This method will calculate the distance that the ball will move on the X-axis.
        /// </summary>
        private void MoveX()
        {
            if (boundingBall.X + velocity.X > screen.Width - boundingBall.Width)
            {
                int distanceOffScreen = boundingBall.X + (int)velocity.X - (screen.Width - boundingBall.Width);
                boundingBall.X = (screen.Width - boundingBall.Width) - distanceOffScreen;
                velocity.X = velocity.X * (-1);

            } else if (boundingBall.X + velocity.X < 0)
            {
                boundingBall.X = -(boundingBall.X + (int)velocity.X);
                velocity.X = velocity.X * (-1);
            }
            else
            {
                boundingBall.X += (int)velocity.X;
            }

        }

        /// <summary>
        /// This method will calculate the distance that the ball will move on the Y-axis.
        /// This method will also check to see if the ball hit the paddle.
        /// </summary>
        private void MoveY()
        {
            if (boundingBall.Y + velocity.Y > screen.Height - boundingBall.Height || boundingBall.Y + velocity.Y > paddle.BoundingBox.Y - boundingBall.Height)
            {

                if (this.boundingBall.Intersects(paddle.BoundingBox))
                {
                    int distanceOffScreen = boundingBall.Y + (int)velocity.Y - (screen.Height - boundingBall.Height);
                    boundingBall.Y = (screen.Height - boundingBall.Height) - distanceOffScreen;
                    velocity.Y = velocity.Y * (-1);
                }
                else
                {
                    boundingBall.Y = screen.Height;
                    velocity.X = 0;
                    velocity.Y = 0;
                }
     
            } else if (boundingBall.Y + velocity.Y < 0)
            {
                boundingBall.Y = -(boundingBall.Y + (int)velocity.Y);
                velocity.Y = velocity.Y * (-1);
            } else
            {
                boundingBall.Y = boundingBall.Y + (int)velocity.Y;
            }
        }
    }
}
