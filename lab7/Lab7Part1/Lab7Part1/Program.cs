﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7Part1
{
    public class Program
    {
        static void Main(string[] args)
        {
            Student a = new Student("Jonathan", 18, 6);
            Student b = new Student("George", 4, 1);
            Student c = new Student("Franco", 56, 2);
            Student d = new Student("Antonio", 67, 7);
            Student e = new Student("Daniella", 22, 9);
            Student f = new Student("Vanessa", 31, 5);
            Student g = new Student("Charles", 74, 5);
            Student h = new Student("Ryan", 2, 6);

            Student[] array = { a, b, c, d, e, f, g, h };
            CompareByAge comparator = new CompareByAge();
            CompareByName comparator2 = new CompareByName();
            IdClosestToStudentComparer comparator3 = new IdClosestToStudentComparer(4);

            array = Sort(array, comparator3);

            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine(array[i].IdNumber);
            }
        }

        public static Student[] Sort(Student[] array, IStudentComparer comparator)
        {
            
            Student temp;

            for (int i = 0; i < array.Length; i++)
            {
                Student min = array[i];
                for (int j = i; j < array.Length; j++)
                {
                    if (comparator.CompareTo(min, array[j]) > 0)
                    {
                        temp = min;
                        min = array[j];
                        array[j] = temp;
                    }
                }
                array[i] = min;
            }
            return array;
        }

    }
}
