﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;

namespace PongLibrary
{
    public class Paddle
    {
        readonly private int speed;
        private int screenWidth;
        private Rectangle boundingBox;

        /// <summary>
        /// This constructor will initialize the readonly variable speed, which represents 
        /// the speed, in pixels, that the ball will travel everytime a move method is 
        /// called. It will also initialize the screenWidth and the boundingBox variables. 
        /// </summary>
        /// <param name="paddleWidth"></param>
        /// <param name="paddleHeight"></param>
        /// <param name="screenWidth"></param>
        /// <param name="screenHeight"></param>
        /// <param name="speed"></param>
        public Paddle(int paddleWidth, int paddleHeight, int screenWidth, int screenHeight, int speed)
        {
            int x = (screenWidth/2) - (paddleWidth/2) ;
            int y = screenHeight - paddleHeight;

            boundingBox = new Rectangle(x, y, paddleWidth, paddleHeight);
            this.screenWidth = screenWidth;
            this.speed = speed;
        }


        /// <summary>
        /// This is a property for the variable boundingBox. This proprety has a private
        /// set and a public get, that returns the value of the Rectangle boundingBox. 
        /// </summary>
        public Rectangle BoundingBox
        {

            get
            {
                return boundingBox;
            }

        }


        /// <summary>
        /// This method is a no param and no return method that will move the paddle to the
        /// left. This method will also ensure that the paddle does not move off the screen.
        /// </summary>
        public void MoveLeft()
        {
            if (boundingBox.X - speed <= 0)
            {
                boundingBox.X = 0;
            } else
            {
                boundingBox.X -= speed; 
            }
        }


        /// <summary>
        /// This method is a no param and no return method that will move the paddle to the 
        /// right of the screen. This method will also ensure that the paddle does not go off
        /// the screen.
        /// </summary>
        public void MoveRight()
        {
            if (boundingBox.X + speed >= screenWidth - boundingBox.Width)
            {
                boundingBox.X = screenWidth - boundingBox.Width;
            } else
            {
                boundingBox.X += speed;
            }
        }


    }
}
