﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7Part1
{
    public interface IStudentComparer
    {
       int CompareTo(Student one, Student two);
    }
}
