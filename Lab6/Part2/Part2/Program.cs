﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part2
{
    // Delegate types
    public delegate bool IntPredicate(int x);
    public delegate void IntAction(int x);

    // Integer lists with Act and Filter operations.
    // An IntList containing the element 7 9 13 may be constructed as 
    // new IntList(7, 9, 13) due to the params modifier.

    class IntList
    {
        List<int> list;

        public IntList(params int[] elements)
        {
            this.list = new List<int>(elements);
        }

        public void Act(IntAction f)
        {
            foreach (int e in list)
            {
                f(e);
            }
        }

        public IntList Filter(IntPredicate p)
        {
            List<int> returnList = new List<int>();
            for (int i = 0; i < list.Count; i++)
            {
                if(p(list[i]))
                {
                    returnList.Add(list[i]);
                }
            }
            return new IntList(returnList.ToArray());
        }
    }

    class MyTest
    {
        public static void Main(String[] args)
        {
            IntList list = new IntList(1,2,3,4,26);
            list.Act(Console.WriteLine);

            IntPredicate even = (x) => { return (x % 2 == 0); };
            IntPredicate greaterThan25 = (x) => { return (x >= 25); };

            IntList evenList = list.Filter(even);
            evenList.Act(Console.WriteLine); 

            IntList greaterList = list.Filter(greaterThan25);
            greaterList.Act(Console.WriteLine);


            IntPredicate odd = (x) => { return (x % 2 == 1); };
            IntPredicate greaterThan5 = (x) => { return (x >= 5); };
            IntPredicate between2And7 = (x) => { return (x > 2 && x < 7); };

            IntList oddList = list.Filter(odd);
            oddList.Act(Console.WriteLine);

            IntList greaterThan5List = list.Filter(greaterThan5);
            greaterThan5List.Act(Console.WriteLine);

            IntList between = list.Filter(between2And7);
            between.Act(Console.WriteLine);

            Console.ReadKey();

        }
    }
}
