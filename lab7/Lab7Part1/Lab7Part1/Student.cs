﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7Part1
{
    public class Student
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public int IdNumber { get; set; }

        public Student(string name, int age, int idNumber)
        {
            Name = name;
            Age = age;
            IdNumber = idNumber;
        }
    }
}
