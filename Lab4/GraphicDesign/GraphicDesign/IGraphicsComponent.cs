﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicDesign
{
    public interface IGraphicsComponent
    {
        string Name { get; set;}
        void Display();
    }
}
