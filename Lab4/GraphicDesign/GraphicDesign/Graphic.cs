﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicDesign
{
    public class Graphic : IGraphicsComponent
    {
        public string Name { get; set; }
        private IList<IGraphicsComponent> components;

        public Graphic(string name)
        {
            Name = name;
            components = new List<IGraphicsComponent>();
        }

        public void AddComponent(IGraphicsComponent toAdd)
        {
            components.Add(toAdd);
        }

        public void RemoveComponent(IGraphicsComponent toRemove)
        {
            components.Remove(toRemove);
        }

        public void Display() {
            Console.WriteLine("\n" + Name);

            foreach (IGraphicsComponent component in components)
            {
                component.Display();
            }
        }
    }
}
