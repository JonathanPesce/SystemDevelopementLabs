﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7Part1
{
    public class CompareByAge : IStudentComparer
    {
        public int CompareTo(Student one, Student two)
        {
            return one.Age - two.Age;
        }
    }
}
