﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using lab11.Models;

namespace lab11.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            List<Address> people;
            using (Lab9Entities db = new Lab9Entities())
            {
                people = (from person in db.Addresses
                              orderby person.LastName, person.FirstName
                              select person).ToList(); 

            }
                return View(people);
        }
    }
}