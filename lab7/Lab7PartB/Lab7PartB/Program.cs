﻿using Lab7Part1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7PartB
{
    class Program
    {
        public delegate int CompareStudent(Student one, Student two);

        public static int NameCompare(Student one, Student two)
        {
            return one.Name.CompareTo(two.Name);
        }

        public static int AgeCompare(Student one, Student two)
        {
            return one.Age - two.Age;
        }

        public static CompareStudent CompareStudentsFrom(int id)
        {
            return (one, two) =>
            {
                return Math.Abs(id - one.IdNumber) - Math.Abs(id - two.IdNumber);
            };
        }

        public static Student[] Sort(Student[] array, CompareStudent del)
        {
                Student temp;

                for (int i = 0; i < array.Length; i++)
                {
                    Student min = array[i];
                    for (int j = i; j < array.Length; j++)
                    {
                        if (del(min, array[j]) > 0)
                        {
                            temp = min;
                            min = array[j];
                            array[j] = temp;
                        }
                    }
                    array[i] = min;
                }
                return array;
        }

        static void Main(string[] args)
        {
            Student a = new Student("Jonathan", 18, 6);
            Student b = new Student("George", 4, 1);
            Student c = new Student("Franco", 56, 2);
            Student d = new Student("Antonio", 67, 7);
            Student e = new Student("Daniella", 22, 9);
            Student f = new Student("Vanessa", 31, 5);
            Student g = new Student("Charles", 74, 5);
            Student h = new Student("Ryan", 2, 6);

            Student[] array = { a, b, c, d, e, f, g, h };

            array = Sort(array, NameCompare);

            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine(array[i].Name);
            }

            Console.WriteLine("**************************************************");

            array = Sort(array, AgeCompare);

            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine(array[i].Age);
            }

            Console.WriteLine("**************************************************");

            array = Sort(array, (x,y) => x.IdNumber - y.IdNumber);

            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine(array[i].IdNumber);
            }

            Console.WriteLine("**************************************************");

            array = Sort(array, (x, y) => Math.Abs(42 - x.Age) - Math.Abs(42 - y.Age));

            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine(array[i].Age);
            }

            Console.WriteLine("**************************************************");

            array = Sort(array, CompareStudentsFrom(6));

            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine(array[i].IdNumber);
            }
        }
    }
}
