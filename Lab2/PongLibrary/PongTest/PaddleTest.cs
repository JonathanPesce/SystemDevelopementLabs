﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;
using PongLibrary;

namespace PongTest
{
    [TestClass]
    public class PaddleTest
    {
        [TestMethod]
        public void MoveLeft_EnoughSpace()
        {
            //arrange
            Paddle paddle = new Paddle(4, 0, 10, 0, 2);

            //act
            int x = MoveLeft(paddle);

            //assert
            Assert.AreEqual(1, x);

        }

        [TestMethod]
        public void MoveLeft_PartialSpace()
        {
            //arrange
            Paddle paddle = new Paddle(4, 0, 10, 0, 4);

            //act
            int x = MoveLeft(paddle);

            //assert
            Assert.AreEqual(0, x);
        }

        [TestMethod]
        public void MoveLeft_NoSpace()
        {
            //arrange
            Paddle paddle = new Paddle(4, 0, 4, 0, 6);

            //act
            int x = MoveLeft(paddle);

            //assert
            Assert.AreEqual(0, x);
        }

        [TestMethod]
        public void MoveRight_EnoughSpace()
        {
            //arrange
            Paddle paddle = new Paddle(4, 0, 10, 0, 2);

            //act
            int x = MoveRight(paddle);

            //assert
            Assert.AreEqual(5, x);
        }


        [TestMethod]
        public void MoveRight_PartialSpace()
        {
            //arrange
            Paddle paddle = new Paddle(4, 0, 10, 0, 4);

            //act
            int x = MoveRight(paddle);

            //assert
            Assert.AreEqual(6, x);
        }

        [TestMethod]
        public void MoveRight_NoSpace()
        {
            //arrange
            Paddle paddle = new Paddle(4, 0, 10, 0, 6);

            //act
            int x = MoveRight(paddle);

            //assert
            Assert.AreEqual(7, 7);
        }


        private int MoveLeft(Paddle paddle)
        {
            paddle.MoveLeft();
            Rectangle temp = paddle.BoundingBox;
            return temp.X;
        }

        private int MoveRight(Paddle paddle)
        {
            paddle.MoveRight();
            Rectangle temp = paddle.BoundingBox;
            return temp.X;
        }
    }
}
