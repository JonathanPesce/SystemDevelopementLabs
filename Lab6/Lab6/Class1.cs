﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab6
{
    class HR
    {
        public delegate void NewEmployee(string name, string employeeId);
        public event NewEmployee newEmployee;

        public void onNewEmployee(string name, string employeeId)
        {
            newEmployee?.Invoke(name, employeeId);
        }

        public void RegisterEmployee(string name, string employeeId)
        {
            // if this was a real application we would do lots of HR related stuff here 
            // raise the event that the new employee is fired here
            onNewEmployee(name, employeeId);
        }
    }

    class Admin
    {
        public Admin(HR hr)
        {
            hr.newEmployee += (x, y) => { Console.WriteLine(x + y); };
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            HR hr = new HR();
            Admin admin = new Admin(hr);
            hr.RegisterEmployee("Joe Kerr", "a112233");
            Console.ReadLine();
        }
    }
}
