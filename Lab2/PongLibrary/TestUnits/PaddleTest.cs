﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using PongLibrary;

namespace TestUnits
{
    class PaddleTest
    {
        static void Main(string[] args)
        {

            Paddle paddle = new Paddle(20, 10, 100, 50, 5);
            Rectangle coords = paddle.BoundingBox;

            Console.WriteLine("The Paddle has been created and should be at points (40, 40)");
            Console.WriteLine("The Paddle's coordinates are at (" + coords.X + ", " + coords.Y + ")");

            paddle.MoveRight();
            coords = paddle.BoundingBox;

            Console.WriteLine();
            Console.WriteLine("Let's move the paddle to the right, the X coordinate should be 45");
            Console.WriteLine("The Paddle's X coordinate is: " + coords.X);

            paddle.MoveLeft();
            paddle.MoveLeft();
            coords = paddle.BoundingBox;

            Console.WriteLine();
            Console.WriteLine("Let's move the paddle to the left twice, the X coordinate should be 35");
            Console.WriteLine("The Paddle's X coordinate is: " + coords.X);


            paddle.MoveLeft();
            paddle.MoveLeft();
            paddle.MoveLeft();
            paddle.MoveLeft();
            paddle.MoveLeft();
            paddle.MoveLeft();
            paddle.MoveLeft();
            paddle.MoveLeft();
            paddle.MoveLeft();
            paddle.MoveLeft();
            paddle.MoveLeft();
            paddle.MoveLeft();
            coords = paddle.BoundingBox;

            Console.WriteLine();
            Console.WriteLine("Let's move the paddle to the left 12 times, the X coordinate should be 0");
            Console.WriteLine("The Paddle's X coordinate is: " + coords.X);

            paddle.MoveRight();
            paddle.MoveRight();
            paddle.MoveRight();
            paddle.MoveRight();
            paddle.MoveRight();
            paddle.MoveRight();
            paddle.MoveRight();
            paddle.MoveRight();
            paddle.MoveRight();
            paddle.MoveRight();
            paddle.MoveRight();
            paddle.MoveRight();
            paddle.MoveRight();
            paddle.MoveRight();
            paddle.MoveRight();
            paddle.MoveRight();
            paddle.MoveRight();
            paddle.MoveRight();
            paddle.MoveRight();
            paddle.MoveRight();
            paddle.MoveRight();
            paddle.MoveRight();
            paddle.MoveRight();
            paddle.MoveRight();
            coords = paddle.BoundingBox;

            Console.WriteLine();
            Console.WriteLine("Let's move the paddle to the right a bunch of time, the X coordinate should be 80");
            Console.WriteLine("The Paddle's X coordinate is: " + coords.X);

           
        }
    }
}
