﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> list = new List<int> { 1, 4, 2, 7, 8, 9 };

            var results =
                from x in list
                where (x % 2 == 0)
                select x * 10;

            results = results.ToList();

            for (int i = 0; i < list.Count; i++)
            {
                list[i] += 12;
            }

            foreach (int x in results)
            {
                Console.WriteLine(x);
            }


            List<Person> listOfPeople = new List<Person>
            {
                new Person{FirstName = "Sri", LastName = "Kuo", Age = 12},
                new Person{FirstName = "Carl", LastName = "Gauss", Age = 18},
                new Person{FirstName = "Ada", LastName = "Lovelace", Age = 2},
                new Person{FirstName = "Sri", LastName = "Ramanujan", Age = 32},
            };

            var resultOfPeople = 
                from p in listOfPeople
                where p.Age >= 13 && p.Age <= 19
                select new {p.FirstName, p.Age};

            foreach(var p in resultOfPeople)
            {
                Console.WriteLine(p.FirstName + " has an age of " + p.Age);
            }

            var oldestMinor = listOfPeople.Where(p => p.Age < 18).Max(p => p.Age);
            Console.WriteLine(oldestMinor);

            var OrderByFirstAndLast = 
                from p in listOfPeople
                orderby p.FirstName, p.LastName ascending
                select p.FirstName + " "  + p.LastName;

            foreach(String p in OrderByFirstAndLast)
            {
                Console.WriteLine(p);
            }

            String[] lines = System.IO.File.ReadAllLines(@"C:\Users\1635333\Desktop\SystemDevelopementLabs\Lab8\Lab8\Lab8\bin");

            String lineWithBreak;
            foreach(string s in lines)
            {
                if (lines.Contains("hard"))
                {
                    lineWithBreak = s;
                    break;
                }
            }
        }
    }
}
