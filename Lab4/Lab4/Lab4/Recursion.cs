﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    public class Recursion
    {
        public static int SumAllPositive(int n)
        {
            if (n < 0)
                throw new ArgumentException("Error - This method must take an int larger than 0");

            if (n == 0)
                return 0;

            return n + SumAllPositive(n - 1);
        }


        public static void PrintFiles(String directory)
        {
            string[] files = Directory.GetFileSystemEntries(directory);

            foreach (string file in files)
            {
                Console.WriteLine(Path.GetFileName(file));

                if (Directory.Exists(file))
                {
                    PrintFiles(file);
                } 
            }
        }

        
    }
}
