﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7Part1
{
    public class IdClosestToStudentComparer : IStudentComparer
    {
        private int id;

        public IdClosestToStudentComparer(int id)
        {
            this.id = id;
        }

        public int CompareTo(Student one, Student two)
        {
            return Math.Abs(this.id - one.IdNumber) - Math.Abs(this.id - two.IdNumber);
        }

    }
}
