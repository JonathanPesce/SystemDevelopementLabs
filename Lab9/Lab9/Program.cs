﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab9
{
    public class Program
    {
        static void Main(string[] args)
        {
            Address jonathan = new Address();
            jonathan.Email = "jonathanpesce@hotmail.com";
            jonathan.FirstName = "Jonathan";
            jonathan.LastName = "Pesce";
            jonathan.PhoneNumber = "514-365-2474";

            Address Daniel = new Address();
            Daniel.Email = "danielrafail@hotmail.com";
            Daniel.FirstName = "Daniel";
            Daniel.LastName = "Rafail";
            Daniel.PhoneNumber = "514-666-6666";

            Address Niv = new Address();
            Niv.Email = "NivDoesn'tKnowTwistAndShout@gmail.com";
            Niv.FirstName = "Niv";
            Niv.LastName = "Doesn'tKnowTwistAndShout";
            Niv.PhoneNumber = "999-999-9999";

            using(AddressDB  db = new AddressDB())
            {

                //Add three users to the database   
                db.Addresses.Add(jonathan);
                db.Addresses.Add(Niv);
                db.Addresses.Add(Daniel);

                db.SaveChanges();

                var users = from user in db.Addresses
                            orderby user.FirstName
                            select user;

                Console.WriteLine("Added three users:");
                foreach(var user in users)
                {
                    Console.WriteLine(user.FirstName + " " + user.LastName);
                }

                //Modify the address of "Jonathan"
                Address addressToChange = (from address in db.Addresses
                                          where address.FirstName.Equals("Jonathan")
                                          select address).FirstOrDefault();

                addressToChange.FirstName = "NameChangedFromJonathan";

                Console.WriteLine("");
                Console.WriteLine("Modify Jonathan:");
                foreach (var user in users)
                {
                    Console.WriteLine(user.FirstName + " " + user.LastName);
                }


                //Delete an entry
                db.Addresses.Remove(Daniel);
                db.SaveChanges();

                Console.WriteLine("");
                Console.WriteLine("THIRD QUERY:");
                foreach (var user in users)
                {
                    Console.WriteLine(user.FirstName + " " + user.LastName);
                }


                //Delete all that was added to the database
                db.Addresses.RemoveRange(db.Addresses);
                db.SaveChanges();
            };

            bool done = false;
            int response = 0;
            do
            {
                do
                {
                    Console.WriteLine("Choose one of the following options:");
                    Console.WriteLine("1: Add user");
                    Console.WriteLine("2: Find user");
                    Console.WriteLine("3: Logout");
                    response = int.Parse(Console.ReadLine());
                } while (response < 0 || response > 4);

                switch (response)
                {
                    case 1:
                        addUser();
                        break;
                    case 2:
                        findUser();
                        break;
                    case 3:
                        Console.WriteLine("Bye. Come again soon.");
                        done = true;
                        break;
                } 

            } while (!done);

        }

        private static void addUser()
        {
            Address personToAdd = new Address();

            Console.WriteLine("Please enter the following information:");

            Console.WriteLine("First Name: ");
            personToAdd.FirstName = Console.ReadLine();

            Console.WriteLine("Last Name: ");
            personToAdd.LastName = Console.ReadLine();

            Console.WriteLine("Email: ");
            personToAdd.Email = Console.ReadLine();

            Console.WriteLine("Phone Number: ");
            personToAdd.PhoneNumber = Console.ReadLine();

            using(AddressDB db = new AddressDB())
            {
                db.Addresses.Add(personToAdd);
                db.SaveChanges();
            };
        }

        private static void findUser()
        {
            Console.WriteLine("Please enter in a last name:");
            string lastName = Console.ReadLine();

            using(AddressDB db = new AddressDB())
            {
                IEnumerable<Address> results = from address in db.Addresses
                                             where address.LastName == lastName
                                             orderby address.LastName, address.FirstName, address.PhoneNumber
                                             select address;

                if (results.Count() == 0)
                {
                    Console.WriteLine("Opps, no users exist with this last name");
                } else
                {
                    Console.WriteLine("Results: ");
                    foreach(Address address in results)
                    {
                        Console.WriteLine(address.FirstName + " " + address.LastName);
                    }
                }
            }
        }
    }
}