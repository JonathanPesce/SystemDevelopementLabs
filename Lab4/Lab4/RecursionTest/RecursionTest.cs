﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lab4;

namespace RecursionTest
{
    [TestClass]
    public class RecursionTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SumAllPositveNegaitveNumber()
        {
            //arrange

            //act
            Recursion.SumAllPositive(-20);

            //assert
        }


        [TestMethod]
        public void SumAllPositveValidInput()
        {
            //arrange
            int result;

            //act
            result = Recursion.SumAllPositive(10);

            //assert
            Assert.AreEqual(55, result);
        }

    }
}
