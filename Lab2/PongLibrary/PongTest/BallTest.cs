﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;
using PongLibrary;

namespace PongTest
{
    [TestClass]
    public class BallTest
    {
        [TestMethod]
        public void TestMoveWithNothingInTheWayX()
        {
            //arrange
            Ball ball = new Ball(4, 4, 10, 20, 1, 1, new Paddle(1, 1, 10, 10, 1));

            //act
            int x = moveX(ball);

            //assert
            Assert.AreEqual(x, 4);
        }

        [TestMethod]
        public void TestMoveWithNothingInTheWayY()
        {
            //arrange
            Ball ball = new Ball(4, 4, 10, 20, 1, 1, new Paddle(1, 1, 10, 10, 1));

            //act
            int y = moveY(ball);

            //assert
            Assert.AreEqual(y, 1);
        }

        [TestMethod]
        public void TestMoveWithRightWall()
        {
            //arrange
            Ball ball = new Ball(4, 4, 6, 20, 3, 1, new Paddle(1, 1, 10, 10, 1));

            //act
            int x = moveX(ball);

            //assert
            Assert.AreEqual(x, 2);
        }

        [TestMethod]
        public void TestMoveWithLeftWall()
        {
            //arrange
            Ball ball = new Ball(4, 4, 6, 20, -3, 1, new Paddle(1, 1, 10, 10, 1));

            //act
            int x = moveX(ball);

            //assert
            Assert.AreEqual(x, 2);
        }

        [TestMethod]
        public void TestMoveWithTopWall()
        {
            //arrange
            Ball ball = new Ball(4, 4, 6, 20, 0, -5, new Paddle(1, 1, 10, 10, 1));

            //act
            int y = moveY(ball);

            //assert
            Assert.AreEqual(y, 5);
        }

        [TestMethod]
        public void TestMoveWithBottomWallNotHittingPaddle()
        {
            //arrange 
            Ball ball = new Ball(4, 4, 8, 8, 0, 6, new Paddle(1, 1, 10, 10, 1));

            //act
            int y = moveY(ball);

            //assert
            Assert.AreEqual(8, y);
        }


        [TestMethod]
        public void TestMoveWithBottomWallHittingPaddle()
        {
            //arrange 
            Ball ball = new Ball(4, 4, 8, 8, 0, 2, new Paddle(8, 3, 8, 8, 1));

            //act
            int y = moveY(ball);

            //assert
            Assert.AreEqual(4, y);
        }

        private int moveX(Ball ball)
        {
            ball.Move();
            return ball.BoundingBox.X;
        }

        private int moveY(Ball ball)
        {
            ball.Move();
            return ball.BoundingBox.Y;
        }


    }
}
