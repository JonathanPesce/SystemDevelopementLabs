﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using PongLibrary;
/*
namespace TestUnits
{
    class BallTest
    {
        static void Main(string[] args)
        {
            Paddle paddle = new Paddle(10, 4, 20, 20, 1);
            Ball ball = new Ball(4, 4, 20, 20, 2, -2, paddle);
            Rectangle coords = ball.BoundingBox;

            ball.Move();
            coords = ball.BoundingBox;
            Console.WriteLine();
            Console.WriteLine("Move 1");
            Console.WriteLine("Expected: (10, 2)");
            Console.WriteLine("Result:   (" + coords.X + "," + " " + coords.Y + ")");

            ball.Move();
            coords = ball.BoundingBox;
            Console.WriteLine();
            Console.WriteLine("Move 2");
            Console.WriteLine("Expected: (12, 4)");
            Console.WriteLine("Result:   (" + coords.X + "," + " " + coords.Y + ")");

            ball.Move();
            coords = ball.BoundingBox;
            Console.WriteLine();
            Console.WriteLine("Move 3");
            Console.WriteLine("Expected: (14 , 6)");
            Console.WriteLine("Result:   (" + coords.X + "," + " " + coords.Y + ")");

            ball.Move();
            coords = ball.BoundingBox;
            Console.WriteLine();
            Console.WriteLine("Move 4");
            Console.WriteLine("Expected: (16, 8)");
            Console.WriteLine("Result:   (" + coords.X + "," + " " + coords.Y + ")");

            ball.Move();
            coords = ball.BoundingBox;
            Console.WriteLine();
            Console.WriteLine("Move 5");
            Console.WriteLine("Expected: (14, 10)");
            Console.WriteLine("Result:   (" + coords.X + "," + " " + coords.Y + ")");

            ball.Move();
            coords = ball.BoundingBox;
            Console.WriteLine();
            Console.WriteLine("Move 6");
            Console.WriteLine("Expected: (12, 12)");
            Console.WriteLine("Result:   (" + coords.X + "," + " " + coords.Y + ")");

            ball.Move();
            coords = ball.BoundingBox;
            Console.WriteLine();
            Console.WriteLine("Move 7");
            Console.WriteLine("Expected: (10, 14)");
            Console.WriteLine("Result:   (" + coords.X + "," + " " + coords.Y + ")");

            ball.Move();
            coords = ball.BoundingBox;
            Console.WriteLine();
            Console.WriteLine("Move 8");
            Console.WriteLine("Expected: (8, 16)");
            Console.WriteLine("Result:   (" + coords.X + "," + " " + coords.Y + ")");

            ball.Move();
            coords = ball.BoundingBox;
            Console.WriteLine();
            Console.WriteLine("Move 9");
            Console.WriteLine("Expected: (6, 14)");
            Console.WriteLine("Result:   (" + coords.X + "," + " " + coords.Y + ")");

            ball.Move();
            coords = ball.BoundingBox;
            Console.WriteLine();
            Console.WriteLine("Move 10");
            Console.WriteLine("Expected: (4, 12)");
            Console.WriteLine("Result:   (" + coords.X + "," + " " + coords.Y + ")");

            ball.Move();
            coords = ball.BoundingBox;
            Console.WriteLine();
            Console.WriteLine("Move 11");
            Console.WriteLine("Expected: (2, 10)");
            Console.WriteLine("Result:   (" + coords.X + "," + " " + coords.Y + ")");

            ball.Move();
            coords = ball.BoundingBox;
            Console.WriteLine();
            Console.WriteLine("Move 12");
            Console.WriteLine("Expected: (0, 8)");
            Console.WriteLine("Result:   (" + coords.X + "," + " " + coords.Y + ")");

            ball.Move();
            coords = ball.BoundingBox;
            Console.WriteLine();
            Console.WriteLine("Move 13");
            Console.WriteLine("Expected: (2, 6)");
            Console.WriteLine("Result:   (" + coords.X + "," + " " + coords.Y + ")");


        }
    }
}
*/