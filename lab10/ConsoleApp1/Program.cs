﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            XNamespace ns = XNamespace.Get("http://search.yahoo.com/mrss/");
            XElement xml = XElement.Load("https://sports.yahoo.com/nhl/rss.xml");
            var nhlArticles = from item in xml.Descendants("item")
                                              select new
                                              {
                                                  Title = GetValueOrNull(item.Element("Title")),
                                                  Description = GetValueOrNull(item.Element("Description")),
                                                  Link = GetValueOrNull(item.Element("Link")),
                                                  Source = GetValueOrNull(item.Element(ns + "Source"))
                                              };

            using (Lab9Entities db = new Lab9Entities())
            {
                foreach (var article in nhlArticles)
                {
                    NhlNew temp = new NhlNew();
                    temp.Title = article.Title;
                    temp.Description = article.Description;
                    temp.Link = article.Link;
                    temp.Source = article.Source;

                    db.NhlNews.Add(temp);
                    db.SaveChanges();
                }
            }

        }

        public static string GetValueOrNull(XElement element)
        {
            if (element == null)
            {
                return null;
            }
            else
            {
                return element.Value;
            }
        }
    }
}
